# Youtube auto-reporter

Firefox addon to (semi)auto-report spamming channels.

Right-click on link to channel (e.g.: in comment's header) and select 'Auto-report this channel\'s comments'. From now on every comment from this channel is considered spam and will be auto-reported.

On slower computer it may hang - just click toolbar icon to toggle reporting Off and again to toggle it on and it should catch up.

List of channels to report will reset when browser is restarted, or addon is disabled.
