
;(function () {


	//
	// Jobs serial engine
	//

	// Adds job(s) to a list
	function job()
	{
		L(arguments,(j)=>{
			j._i = job._i;
			job._l.push(j);
			job._i ++;
		});
	}
	// Process one job at a time
	job.upd = function () {
		var j;

		if (job._run) {
			j = job._l[0];
			if (j && j.update(j)) {
				// Job finished - pop it
				job._l.shift();
			}
		}

		setTimeout(job.upd, 100);
	};
	job._l = [];
	job._i = 0
	job._run = 1;
	job.upd();



	function checkAuthor(url)
	{
		if (checkAuthor._peel.indexOf(url) >= 0)
			return 0;
		return 1;
	}
	checkAuthor._peel = [// URL list of malicious channels - they need to be peeled
	];

	function check(e)
	{
		var aut_e = Q('a#author-text', e)[0];
		if (aut_e && !checkAuthor(aut_e.href)) return 0;

		return 1;
	}
	function report(e)
	{
		var _e;

		job(
			{
				desc: 'Open menu',
				e: e,
				update: (t)=>{
					Q('ytd-menu-renderer button',t.e)[0].click();
					return 1;
				}
			},
			{
				desc: 'Click "report"',
				update: ()=>{
					var but = Q('.ytd-menu-popup-renderer[role=menuitem]')[0];
					if (but) {
						but.click();
						return 1;
					}
				}
			},
			{
				desc: 'Select "spam"',
				update: ()=>{
					var win = Q('tp-yt-paper-dialog')[0];
					var but = Q('tp-yt-paper-radio-button[role=radio]')[0];// 'Spam' radio button
					if (win && win.style.display == '' && but) {
						but.click();
						return 1;
					}
				}
			},
			{
				desc: 'Submit',
				update: ()=>{
					var e_l = Q('yt-report-form-modal-renderer div.yt-spec-touch-feedback-shape.yt-spec-touch-feedback-shape--touch-response');
					if (e_l.length == 3) {
						e_l[1].click();

						report.cnt ++;
						localStorage.spamDragonSlayer = report.cnt;

						return 1;
					}
				}
			},
			{
				desc: 'OK response',
				update: ()=>{
					var win = Q('tp-yt-paper-dialog')[1];
					var but = Q('tp-yt-paper-dialog div.yt-spec-touch-feedback-shape.yt-spec-touch-feedback-shape--touch-response')[2];
					if (win && win.style.display == '' && but) {
						but.click();
						return 1;
					}
				}
			},
			{
				desc: 'Remove element',
				e: e,
				update: ()=>{
					//e.remove();
					var  i = monitor.e_l.indexOf(e)
					if (i >= 0)
						monitor.e_l.splice(i,1);
					return 1;
				}
			},
		);
	}
	report.cnt = parseInt(localStorage.spamDragonSlayer)||0;

	function monitor()
	{
		LQ('ytd-comment-renderer',(e)=>{
			if (monitor.e_l.indexOf(e) >= 0)
				// We've already checked this
				return;

			if (!check(e)) {
				// Bad comment, bad!
				W('evil found',e.innerText.trim())
				report(e);
			}

			monitor.e_l.push(e);
		});
	}
	monitor.e_l = [];

	function add(url)
	{
		if (monitor.e_l.indexOf(url) >= 0)
			// Already in list
			return;

		// Remove all agains-new-url-unchecked elements (only spam remain)
		monitor.e_l = L(monitor.e_l,(e)=>{
			if (!check(e))
				return e;
		});

		checkAuthor._peel.push(url);
	}
	monitor.add = add;

	function expand()
	{
		LQ('div.more-button',(e)=>{
			if (expand.e_l.indexOf(e) >= 0)
				// Already processed
				return;

			if (e.hidden == false)
				e.click()

			expand.e_l.push(e);
		})
	}
	expand.e_l = [];

	function loadMore()
	{
		LQ('div#button.style-scope.ytd-continuation-item-renderer button.yt-spec-button-shape-next',(o)=>{
			o.click();
		});
	}

	function upd()
	{
		if (upd.last_url != location.href) {
			// Reset
			expand.e_l = [];
			monitor.e_l=[];
			upd.last_url = location.href;
		}
		if (upd._run) {
			loadMore();
			expand();
			monitor();
		}
		setTimeout(upd, 2000);//P(job._l)
	}
	upd._run = 0;
	job._run = 0;
	upd();

	window.ytar = {
		upd:upd,
		job:job,
		monitor:monitor,
		expand:expand,
		checkAuthor:checkAuthor,
	};
})();

browser.runtime.onMessage.addListener(function (m) {
	if (m.b !== undefined) {
		// On/Off toggle

		ytar.upd._run = m.b;
		ytar.job._run = m.b;

		if (!m.b) {
			ytar.job._l = [];
			ytar.monitor.e_l = [];
		}
	}
	if (m.add !== undefined) {
		// Add channel url

		ytar.monitor.add(m.add);
	}
	P(m)
})
