var slay_b = 0;

function render()
{
	if (!slay_b) {
		browser.browserAction.setIcon({path:"youtube-auto-reporter_off.svg"})
		browser.browserAction.setBadgeText({text:'off'})
	} else {
		browser.browserAction.setIcon({path:"youtube-auto-reporter.svg"})
		browser.browserAction.setBadgeText({text:''})
	}
}
browser.browserAction.onClicked.addListener(() => {
	slay_b = !slay_b;
	browser.tabs.query({url:"*://www.youtube.com/*"}).then((tabs)=>{
		var i;
		for (i=0; i<tabs.length; i++)
			browser.tabs.sendMessage(tabs[i].id,{b:slay_b})
	});
	render();
});

browser.contextMenus.create({
	id:'ytar',
	title:'&Auto-report this channel\'s comments',
	contexts:['link'],
	targetUrlPatterns:['*://www.youtube.com/*']
});
browser.contextMenus.onClicked.addListener(function (info,tab) {
	if (info.menuItemId=='ytar') {
		browser.tabs.query({url:"*://www.youtube.com/*"}).then((tabs)=>{
			var i;
			for (i=0; i<tabs.length; i++)
				browser.tabs.sendMessage(tabs[i].id,{add:info.linkUrl})
		});
	}
});

browser.browserAction.setBadgeBackgroundColor({color:'#333'})
render();
